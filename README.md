# tslock

_transparent simple screen locker utility for X._

## Dependencies

- [Xlib](https://www.x.org/releases/current/doc/libX11/libX11/libX11.html)

## Installation

```sh
make clean install
```

## Acknowledgement

Based on [Christopher Jeffrey's fork](https://github.com/chjj/slock) of [slock](https://tools.suckless.org/slock).

## License

SPDX-License-Identifier: [GPL-2.0-or-later](https://spdx.org/licenses/GPL-2.0-or-later.html)

See [LICENSE](LICENSE) file for copyright and license details.

## Reference

- [Stack Overflow: 32bit window depth](https://stackoverflow.com/q/3645632/8507637)

- [Transset Source Code](https://github.com/freedesktop/transset)
